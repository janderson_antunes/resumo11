Páginas: 91, 173

Joined relations
====

Relação `loan`

   loan-number | branch-name | amount 
   --- |--- | ---   
   L-170 | Downtown	| 3000
   L-230 | Redwood  | 4000
   L-260 | Perryridge |	1700

Relação `borrower`

   customer-name | loan-number
   --- | --- 
   Jones | L-170
   Smith | L-230
   Hayes | L-155

   
## Tipos de Join

- condition
- natural
- outer


### Exemplo 1 - *Inner Join*
```
#!sql
loan inner join borrower on loan.loan-number = borrower.loan-number
```

A expressão calcula o *theta join* das relações *loan* e *borrower*, sendo 
a condição de join `loan.loan-numer = borrower.loan-number`. Os atributos 
do resultado consitem dos atributos da relação esquerda, *loan*, seguindos 
dos atributos da relação direita, *borrower*.


loan-number | branch-name | amount | customer-name | loan-number
--- |--- | --- | --- | --- 
L-170 | Downtown| 3000  | Jones | L-170
L-230 | Redwood  | 4000 | Smith | L-230
Resultado do *inner join*


### Exemplo 2 - *Left Outer Join*
```
#!sql
loan left outer join borrower on loan.loan-number = borrower.loan-number
```


loan-number | branch-name | amount | customer-name | loan-number
--- |--- | --- | --- | --- 
L-170 | Downtown| 3000  | Jones | L-170
L-230 | Redwood  | 4000 | Smith | L-230
L-260 | Perryridge | 1700 | null | null
Resultado do *left outer join*

Como este resultado é montado?

1. Primeiro calcule o *inner join*.
2. Para cada tupla *e* na relação esquerda que não *casa* com nenhuma 
tupla da relação direita adicione uma tupla *r* ao resultado do *join*:
    - os atributos da tupla *r* que são derivados da relação esquerda são 
 preenchidos com valores da tupla *e*, e os atributos restantes são
 preenchidos com *null*.

## Junção Cartesiana ou Produto Cartesiano

É o tipo mais simples de junção (*join*). É também conhecida como junção cruzada (*cross join*) e junção produto(*cross product*).

O produto cartesiano toma cada valor na primeira tabela e os pareia com cada valor da segunda tabela.

### Exemplo

Suponha que você tem uma tabela de nomes de crianças e outra tabela com os brinquedos que estas crianças possuem.


**Brinquedos**
***
toy_id | toy
--- | :---
1 | hula hoop
2 | balsa glider
3 | toy soldiers
4 | harmonica
5 | baseball cards
6 | tinker toys
7 | etch-a-sketch
8 | slinky

**Crianças**
***
boy_id | boy | toy_id
--- | --- | ---
1 | Davey | 3
2 | Bobby | 5
3 | Beaver | 2
4 | Richie | 1
6 | Johnny | 4
5 | Billy | 2



```
#!sql
SELECT t.toy, b.boy
FROM toys AS t
   CROSS JOIN
   boys AS b;

```



#### Resultado
***
toy | boy
--- | ---
hula hoop | Davey
hula hoop | Bobby
hula hoop | Beaver
hula hoop | Richie
hula hoop | Johnny
hula hoop | Billy
balsa glider | Davey
balsa glider | Bobby
balsa glider | Beaver
balsa glider | Richie
balsa glider | Johnny
balsa glider | Billy
toy soldiers | Davey
toy soldiers | Bobby
toy soldiers | Beaver
toy soldiers | Richie
toy soldiers | Johnny
toy soldiers | Billy
harmonica | Davey
harmonica | Bobby
harmonica | Beaver
harmonica | Richie
harmonica | Johnny
harmonica | Billy
baseball cards | Davey
baseball cards | Bobby
baseball cards | Beaver
baseball cards | Richie
baseball cards | Johnny
baseball cards | Billy
tinker toys | Davey
tinker toys | Bobby
tinker toys | Beaver
tinker toys | Richie
tinker toys | Johnny
tinker toys | Billy
etch-a-sketch | Davey
etch-a-sketch | Bobby
etch-a-sketch | Beaver
etch-a-sketch | Richie
etch-a-sketch | Johnny
etch-a-sketch | Billy
slinky | Davey
slinky | Bobby
slinky | Beaver
slinky | Richie
slinky | Johnny
slinky | Billy


A seguinte *query* produz o mesmo resultado:

```
#!sql
SELECT toys.toy, boys.boy
FROM toys, boys;

```

## Inner Join

Um INNER JOIN é um CROSS JOIN com algumas linhas removidas por uma condição. Um *inner join* combinas os registros de duas tabelas usando operadores de comparação em uma condição.

**Sintaxe básica**

```
#!sql
SELECT algumascolunas
FROM tabela1
  INNER JOIN
  tabela2
ON algumacondição;

```

Quais brinquedos cada criança possui?

```
#!sql
SELECT boys.boy, toys.toy
FROM boys
  INNER JOIN
  toys
ON boys.toy_id = toys.toy_id;

```

**Resultado**

boy | toy
--- | :----
Richie | hula hoop
Beaver | balsa glider
Billy | balsa glider
Davey | toy soldiers
Johnny | harmonica
Bobby | baseball cards



### Natural Join

É um *inner join** que combina duas tabelas que compartilham o mesmo nome de colula.

Seria possível fazer uma junção natural entre *boys* e *toys*?

> Page 406.







