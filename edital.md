Anexo III
====

## CONTEÚDO PROGRAMÁTICO

### Conhecimentos  gerais

#### Analista Judiciário – Área Apoio Especializado – Especialidade Tecnologia da Informação

##### Português

- Ortografia  oficial.  
  - Acentuação  gráfica.  
  - Flexão  nominal  e  verbal.
  - **Pronomes**:  emprego,  formas  de tratamento e colocação.
  - Emprego de tempos e modos verbais. 
  - Vozes do verbo.
  - Concordância nominal e verbal. 
  - Regência  nominal  e  verbal.
  - Ocorrência  de  crase.
  - Pontuação.  
  - Redação  (confronto  e  reconhecimento  de  frases 
corretas e incorretas). 
  - Intelecção de texto. 

##### Raciocínio lógico

Esta prova visa avaliar a habilidade do candidato em entender a estrutura lógica de relações 
arbitrárias  entre  pessoas,  lugares,  objetos  ou  eventos  fictícios; **deduzir  novas  informações**  das  relações 
fornecidas e avaliar as condições usadas para estabelecer a estrutura daquelas relações. Os estímulos visuais 
utilizados na prova, constituídos de elementos conhecidos e significativos, visam a analisar as habilidades dos 
candidatos para compreender e elaborar a lógica de uma situação, utilizando as funções intelectuais: 
- raciocínio verbal
- raciocínio  matemático
- raciocínio  seqüencial
- orientação  espacial  e  temporal
- formação  de  conceitos, 
-  discriminação  de  elementos. 

Em  síntese,  as  questões  da  prova  destinam-se  a  medir  a  capacidade  de 
compreender o processo lógico que, a partir de um conjunto de hipóteses, conduz, de forma válida, a conclusões 
determinadas. 

### Conhecimentos  específicos

#### Analista Judiciário – Área Apoio Especializado – Especialidade Tecnologia da Informação

- **Organização e arquitetura de computadores**: hardware, software, sistema operacional, dispositivos de entrada e 
saída, periféricos, memória, processador, dispositivos de armazenamento.
- **Redes de  armazenamento de dados**:
conceitos  de  SAN, NAS.  
- **Sistemas operacionais**: conceitos e configurações básicas de Windows 7 Vista,  2003 
Server,  2008  Server  e  Linux  (Red  Hat,  SuSE).
- Gerenciamento  de  memória,  programas,  processos,  entrada  e 
saída.
- Gerenciamento  de  sistemas  de  arquivos  CIFS  e  NFS. 
- Administração  de  usuários,  grupos,  permissões, controles  de  acesso  (LDAP, Active  Directory).
- **Redes  de  computadores**:  conceitos  de  comunicação  de  dados, 
meios  de  transmissão  (Redes  WAN),  cabeamento  estruturado,  redes  sem  fio.  Modelo  OSI.  Protocolo  TCP/IP 
versões  4  e  6.
- **Gerenciamento  de  redes  de  computadores**:  conceitos,  protocolo  SNMP,  qualidade  de  serviço 
(QoS)
- Noções de Data Center
- **Segurança**: criptografia simétrica e assimétrica, certificação e assinatura digital, 
firewall,  filtro  de  conteúdo,  NAT,  VPN,  vírus  de  computador  e  outros  tipos  de  malware.
- **Sistemas  de  cópia  de 
segurança**: tipos, meios de armazenamento.
- Auditoria. 
- Plano de Contingência.
- Normas de segurança ISO 27001 e ISO 27002.
- **Banco de dados**
  - banco de dados relacional
  - modelos E-R
  - linguagens SQL e PL/SQL.
  -  Banco de dados  Oracle  11g.  Conceitos  de  data  warehouse,  data  mining,  OLAP,  portais.
- **Linguagens  de  programação**
  - estrutura de dados
  - algoritmos
  - interpretação e compilação de programação
  - lógica de programação.
  - Orientação a objetos: classe, herança, polimorfismo, objeto. 
- **Engenharia de software**
  - análise e projeto estruturado e orientado a  objetos. 
  - UML.  
- Conceitos  de  HTML,  XHTML,  CSS  e  XML. 
- Modelagem  funcional  e  de  dados 
- Testes, homologação  e  implantação  de  sistemas;  
- Ambientes  e  linguagens  de  programação:  Java,  JavaScript,  PHP, 
Oracle  Forms  e  Reports,  Ruby,  Python.
- Servidores  de  Aplicação\WEB:  Tomcat,  Jboss.  GlassFish,  Apache 
HTTPD  e  IIS
- Frameworks:  EJB,  JSF,  Hibernate  JPA,  J2EE,  JUnit
- Métricas  de  software:  pontos  de  função, pontos  de  casos  de  uso. 
- Arquitetura  de  sistemas:  cliente/servidor,  multicamadas.  
- Webservices  e  arquitetura orientada  a  serviços  (SOA). 
-  Padrões  de  projetos  (design  patterns). 
- **Gestão  de  Tecnologia  da  Informação**
  - Frameworks  ITIL  V3  e  COBIT  (versão  4.2). 
- Qualidade  de  software:  modelos  CMMI,  MPS-BR,  ISO  12207.
- Gerenciamento de projetos: PMBoK
- Inglês Técnico. 







